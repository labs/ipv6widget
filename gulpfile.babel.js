import "@babel/polyfill";
import gulp from "gulp";
import webpackStream from "webpack-stream";
import sass from "gulp-sass";
import autoprefixer from "gulp-autoprefixer";
import concat from "gulp-concat";
import imagemin from "gulp-imagemin";
import csso from "gulp-csso";
import sourcemaps from "gulp-sourcemaps";
import request from "request";
import source from "vinyl-source-stream";
import webpack from "webpack";
import connect from "gulp-connect";

const mode = require("gulp-mode")({
    modes: ["production", "development"],
    default: "development"
});

const skins = ["default", "cz.nic"];

gulp.task("sass", async () => skins.forEach(async skin =>
    gulp.src([
        "src/style/base.scss",
        `src/style/skins/${skin}/skin-${skin}.scss`
    ])
        .pipe(mode.development(sourcemaps.init()))
        .pipe(sass().on("error", sass.logError))
        .pipe(autoprefixer())
        .pipe(mode.production(csso()))
        .pipe(concat(`ipv6widget-${skin}.css`))
        .pipe(mode.development(sourcemaps.write("./")))
        .pipe(gulp.dest("build/"))
));

gulp.task("images", async () =>
    gulp.src(["src/img/**/*svg"])
        .pipe(mode.production(imagemin([imagemin.svgo()])))
        .pipe(gulp.dest("build/img/"))
);

gulp.task("fonts", async () =>
    gulp.src("src/fonts/**/*")
        .pipe(gulp.dest("build/fonts/"))
);

gulp.task("html", async () =>
    gulp.src("src/html/*")
        .pipe(gulp.dest("build/"))
);

gulp.task("js", async () => {
    const webpackConf = require("./webpack.config.js");
    return gulp.src(["src/js/index.jsx", "src/js/wrapper.js"])
        .pipe(webpackStream(webpackConf, webpack))
        .pipe(gulp.dest("build/"))
    && request("https://cdnjs.cloudflare.com/ajax/libs/raven.js/3.26.2/raven.min.js")
        .pipe(source("raven.min.js"))
        .pipe(gulp.dest("build/"))
    && request("https://cdnjs.cloudflare.com/ajax/libs/raven.js/3.26.2/raven.min.js.map")
        .pipe(source("raven.min.js.map"))
        .pipe(gulp.dest("build/"));
});

gulp.task("copy-php", () =>
    gulp.src("src/backend/*")
        .pipe(gulp.dest("build/"))
);

gulp.task("build", gulp.parallel("sass", "images", "fonts", "js", "html", "copy-php"));

gulp.task("watch", async () => (
    gulp.watch("src/style/**/*.scss", gulp.series("sass"))
    && gulp.watch("src/images/**/*", gulp.series("images"))
    && gulp.watch("src/js/**/*.jsx", gulp.series("js"))
    && gulp.watch("src/fonts/**/*", gulp.series("fonts"))
    && gulp.watch("src/html/**/*", gulp.series("html"))
));

gulp.task("devserver", gulp.parallel("build", "watch", async () =>
    connect.server({
        root: "./build",
        port: 3000
    })
));
