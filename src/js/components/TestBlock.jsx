import { h, Component } from "preact";

export default class TestBlock extends Component {
    constructor(props) {
        super(props);
        this.state = {
            status: this.props.test.getInitialState(this.props.options)
        };
    }

    componentWillMount() {
        const component = this;
        if (!this.props.result) {
            this.props.test.getState(this.props.options, this.props.results).then((status) => {
                component.props.storeTestResult(this.props.name, status);
                component.setState({ status });
            });
        } else {
            component.setState({ status: this.props.result });
        }
    }

    render() {
        let infoButton, childrenTests;

        if (this.props.result && this.props.options.info) {
            infoButton = (
                <button
                    className="info-button"
                    onClick={() => this.props.showInfo(this.props.name)}
                >
                    info
                </button>
            );
        }

        if (this.props.options.children && this.props.options.children.length > 0) {
            let childrenClass;
            if (this.props.options.showChildren === true) {
                childrenClass = "children-tests";
            } else {
                childrenClass = "children-tests hidden";
            }
            childrenTests = (
                <div class={childrenClass}>
                    {this.props.options.children.map((testName, i) => { // for each enabled test
                        const testClass = require(`../tests/${testName}.jsx`).default.prototype; // load it's class from ../tests/name.jsx
                        // …and pass it to TestBlock, along with other props:
                        return (
                            <TestBlock
                                key={testName}
                                name={testName}
                                test={testClass}
                                options={this.props.testSettings[testName]}
                                lang={this.props.lang}
                                showInfo={this.props.showInfo}
                                storeTestResult={this.props.storeTestResult}
                                label={this.props.i18n.tests[testName].label}
                                i18n={this.props.i18n}
                                results={this.props.results}
                                result={this.props.results[testName]}
                            />
                        );
                    })}
                </div>
            );
        }

        return (
            <div className={`test ${this.state.status}`}>
                <div className="status" />
                <div className="label">
                    <span>{this.props.label[this.state.status][this.props.lang]}</span>
                    {childrenTests}
                </div>
                {infoButton}
            </div>
        );
    }
}
