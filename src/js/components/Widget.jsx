import { h, Component } from "preact";
import WidgetHeader from "./WidgetHeader.jsx";
import Tests from "./Tests.jsx";
import Speed from "./Speed.jsx";
import Info from "./Info.jsx";

export default class Widget extends Component {
    constructor(props) {
        super(props);
        this.showInfo = this.showInfo.bind(this);
        this.showTests = this.showTests.bind(this);
        this.storeTestResult = this.storeTestResult.bind(this);
        this.storeSpeedResult = this.storeSpeedResult.bind(this);
        this.showSpeedtests = this.showSpeedtests.bind(this);
        this.state = {
            showing: "tests",
            results: {},
            speed: {},
            speedtestsShown: false
        };
    }

    storeTestResult(name, state) {
        const st = this.state;
        st.results[name] = state;
        this.setState(st);
    }

    storeSpeedResult(name, direction, speed) {
        const st = this.state;
        if (!st.speed[name]) {
            st.speed[name] = {};
        }
        st.speed[name][direction] = speed;
        this.setState(st);
    }

    showInfo(testName) {
        this.setState({
            showing: "info",
            infoFor: testName
        });
    }

    showSpeedtests() {
        this.setState({
            speedtestsShown: true
        });
    }

    showTests() {
        const st = this.state;
        delete st.infoFor;
        st.showing = "tests";
        this.setState(st);
    }

    logState() {
        console.info(JSON.parse(JSON.stringify(this.state))); // eslint-disable-line no-console
    }

    render() {
        let widgetHeader,
            testComponent,
            widgetContent,
            speedComponent;

        let language = this.props.options.lang // use language from options if given
                       || document.documentElement.lang // if not, try to use document language (<html lang='xx'>) if available
                       || window.navigator.language.replace(/-.*/, "") // if not, try to use browser language
                       || window.navigator.userLanguage.replace(/-.*/, "") // this is the same thing for IE
                       || "en"; // if everything fails, just resort to English

        if (typeof this.props.i18n.header[language] !== "string") { // if an unsupported language is set, use English
            language = "en";
        }

        if (this.props.options.header !== false) { // don't render header if asked not to
            widgetHeader = (<WidgetHeader label={this.props.i18n.header} lang={language} />);
        }

        if (this.props.options.tests && this.props.options.tests.length > 0) { // if there are any tests enabled, inlude Tests component
            testComponent = (
                <Tests
                    tests={this.props.options.tests}
                    showInfo={this.showInfo}
                    storeTestResult={this.storeTestResult}
                    testSettings={this.props.testSettings}
                    i18n={this.props.i18n}
                    lang={language}
                    results={this.state.results}
                />
            );
        }

        if (this.props.options.speed && this.props.options.speed.length > 0) { // … same for speed tests
            speedComponent = (
                <Speed
                    tests={this.props.options.speed}
                    storeSpeedResult={this.storeSpeedResult}
                    i18n={this.props.i18n}
                    lang={language}
                    results={this.state.speed}
                    showSpeedtests={this.showSpeedtests}
                    speedtestsShown={this.state.speedtestsShown}
                    speedSettings={this.props.speedSettings}
                />
            );
        }

        switch (this.state.showing) {
            case "tests":
                widgetContent = (
                    <div className="ipv6widget">
                        {widgetHeader}
                        <div className="widget-content">
                            {testComponent}
                            {speedComponent}
                        </div>
                    </div>
                );
                break;
            case "info":
                if (!this.state.results[this.state.infoFor]) {
                    throw Error(`Unexpected Widget state (infoFor): ${this.state.showing}`);
                }
                widgetContent = (
                    <div className="ipv6widget">
                        <Info
                            showTests={this.showTests}
                            i18n={this.props.i18n.tests[this.state.infoFor]}
                            lang={language}
                            testName={this.state.infoFor}
                            testState={this.state.results[this.state.infoFor]}
                        />
                    </div>
                );
                break;
            default:
                throw Error(`Unexpected Widget state (infoFor): ${this.state.showing}`);
        }

        return widgetContent;
    }
}
