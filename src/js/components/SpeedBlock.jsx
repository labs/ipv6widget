import { h, Component } from "preact";

export default class SpeedBlock extends Component {
    constructor(props) {
        super(props);
        this.runSpeedtest = this.runSpeedtest.bind(this);
    }

    formatSpeed(speed) {
        if (speed === null || isNaN(speed)) {
            return "";
        }
        let formatted, unit;
        if (speed >= 1) {
            formatted = (Math.round(speed * 100) / 100).toString();
            unit = "Mb/s";
        } else {
            formatted = (Math.round(speed * 1000 * 100) / 100).toString();
            unit = "Kb/s";
        }
        if (formatted.length < 5) {
            formatted = formatted.replace(/([,.][0-9]+)$/, "$10");
        }
        return `${formatted} ${unit}`;
    }

    runSpeedtest() {
        const component = this;
        return new Promise(function (resolve, reject) {
            if (!component.props.results || !component.props.results.download || !component.props.results.upload) {
                component.props.storeSpeedResult(component.props.name, "status", "downloading");
                return component.props.test.getDownloadSpeed(component.props.settings.download).then((dlspeed) => {
                    component.props.storeSpeedResult(component.props.name, "download", dlspeed);
                }).then(() => {
                    component.props.storeSpeedResult(component.props.name, "status", "uploading");
                    return component.props.test.getUploadSpeed(component.props.settings.upload).then((ulspeed) => {
                        component.props.storeSpeedResult(component.props.name, "upload", ulspeed);
                        component.props.storeSpeedResult(component.props.name, "status", "ok");
                        resolve();
                    });
                }).catch(function (e) {
                    component.props.storeSpeedResult(component.props.name, "status", "fail");
                    resolve();
                });
            } else {
                resolve();
            }
        });
    }

    render() {
        let infoButton, download, upload, status;

        if (this.props.results) {
            download = this.props.results.download || null;
            upload = this.props.results.upload || null;
        } else {
            download = null;
            upload = null;
        }

        if (this.props.results && this.props.results.status) {
            status = this.props.results.status;
        } else {
            status = this.props.test.getInitialState(this.props.settings);
        }

        return (
            <div className={`speed ${status}`}>
                <div className="status" />
                <div className="label">{this.props.label}</div>
                <div className="speed-download">{this.formatSpeed(download)}</div>
                <div className="speed-upload">{this.formatSpeed(upload)}</div>
                {infoButton}
            </div>
        );
    }
}
