import { h, Component } from "preact";

export default class WidgetHeader extends Component {
    render() {
        return (
            <header className="header">
                <h1>{this.props.label[this.props.lang]}</h1>
            </header>
        );
    }
}
