import { h, Component } from "preact";
import SpeedBlock from "./SpeedBlock.jsx";

export default class Speed extends Component {
    runSpeedtests(event) {
        this.props.showSpeedtests();
        event.target.blur();
        event.target.disabled = true;
    }

    render() {
        let speedBlockClassname = "speed-container";
        let speedButtonClassname = "speed-button";

        if (this.props.speedtestsShown === false) {
            speedBlockClassname += " hidden";
        } else {
            speedButtonClassname += " hidden";
        }

        const speedBlock = (
            <div className={speedBlockClassname}>
                {this.props.tests.map((testName, i) => { // for each enabled speedtest
                    // load it's class from ../speed/name.jsx
                    const testClass = require(`../speed/${testName}.jsx`).default.prototype;
                    // …and pass it to SpeedBlock, along with other props:
                    return (
                        <SpeedBlock
                            key={testName}
                            ref={(ref) => {
                                if (!this.refs) {
                                    this.refs = [ref];
                                } else if (this.refs.length < this.props.tests.length) {
                                    this.refs = this.refs.concat([ref]);
                                }
                                return this.refs;
                            }}
                            name={testName}
                            test={testClass}
                            results={this.props.results[testName]}
                            storeSpeedResult={this.props.storeSpeedResult}
                            settings={this.props.speedSettings[testName]}
                            label={this.props.i18n.speed[testName].label[this.props.lang]}
                        />
                    );
                })}
            </div>);

        // when speedtests are shown and there are no results…
        if (this.props.speedtestsShown === true && Object.keys(this.props.results).length === 0) {
            // …sequentially trigger the runSpeedtest functions of SpeedBlock components:
            const speedtestRunners = this.refs.map(component => component.runSpeedtest);
            speedtestRunners.reduce((resultPromise, runner) => resultPromise.then(runner), Promise.resolve());
        }

        return (
            <div className="speed-container">
                <button class={speedButtonClassname} onClick={this.runSpeedtests.bind(this)}>{this.props.i18n.speed.buttonLabel[this.props.lang]}</button>
                {speedBlock}
            </div>
        );
    }
}
