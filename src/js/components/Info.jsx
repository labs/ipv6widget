import { h, Component } from "preact";

function markdownLinks(text) {
    return text.replace(/\[([^\]]+)\]\(([^ )]+)\)/g, "<a href='$2' target='_blank'>$1</a>");
}

export default class Info extends Component {
    render() {
        return (
            <div className="info-container">
                <header className="info-header">
                    <button class="back-to-test" onClick={this.props.showTests} />
                    <h1>{this.props.i18n.label[this.props.testState][this.props.lang]}</h1>
                </header>
                <div class="info-content">
                    <p dangerouslySetInnerHTML={{ __html: markdownLinks(this.props.i18n.info[this.props.testState][this.props.lang]) }} />
                </div>
            </div>
        );
    }
}
