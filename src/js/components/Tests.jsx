import { h, Component } from "preact";
import TestBlock from "./TestBlock.jsx";

export default class Tests extends Component {
    render() {
        return (
            <div className="test-container">
                {this.props.tests.map((testName, i) => {
                    // for each enabled test
                    const testClass = require(`../tests/${testName}.jsx`).default.prototype; // load it's class from ../tests/name.jsx
                    // …and pass it to TestBlock, along with other props:
                    return (
                        <TestBlock
                            key={testName}
                            name={testName}
                            test={testClass}
                            options={this.props.testSettings[testName]}
                            testSettings={this.props.testSettings}
                            lang={this.props.lang}
                            showInfo={this.props.showInfo}
                            storeTestResult={this.props.storeTestResult}
                            label={this.props.i18n.tests[testName].label}
                            i18n={this.props.i18n}
                            results={this.props.results}
                            result={this.props.results[testName]}
                        />
                    );
                })}
            </div>
        );
    }
}
