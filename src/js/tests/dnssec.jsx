import Test from "./Test.jsx";

export default class dnssec extends Test {
    getState(options, results) {
        const dnssecClass = this;
        return new Promise(function (resolve) {
            setTimeout(function () {
                if (results.rsa === "ok" && results.ecdsa === "ok") {
                    resolve("ok");
                } else if (results.rsa === "fail" && results.ecdsa === "fail") {
                    resolve("fail");
                } else if (results.rsa === "fail" || results.ecdsa === "fail") {
                    resolve("partial");
                } else {
                    resolve(dnssecClass.getState(options, results));
                }
            }, 250);
        });
    }
}
