import "isomorphic-fetch";
import Test from "./Test.jsx";

export default class ipv6 extends Test {
    getState(options) {
        return new Promise(function (resolve) {
            fetch(options.url, {
                mode: "no-cors"
            }).then(() => {
                resolve("ok");
            }).catch(() => {
                resolve("fail");
            });
        });
    }
}
