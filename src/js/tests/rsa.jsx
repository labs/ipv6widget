import "isomorphic-fetch";
import Test from "./Test.jsx";

export default class rsa extends Test {
    getState(options) {
        return new Promise(function (resolve) {
            fetch(options.url, {
                mode: "no-cors"
            }).then(() => {
                resolve("fail");
            }).catch(() => {
                resolve("ok");
            });
        });
    }
}
