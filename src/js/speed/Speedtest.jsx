import "isomorphic-fetch";

export default class Speedtest {
    getInitialState(options) {
        return options.initialState || "waiting";
    }

    getDownloadSpeed(options) {
        return this.measureDownloadSpeed(0, options);
    }

    getUploadSpeed(options) {
        return this.measureUploadSpeed(options.url, options.initialSize, options.minDuration, options.maxSize);
    }

    uploadData(data, url) {
        return new Promise(function (resolve, reject) {
            fetch(url, {
                method: "POST",
                body: data
            }).then(function (response) {
                resolve(performance.now());
            }).catch(function (e) {
                reject(e);
            });
        });
    }

    measureUploadSpeed(url, data, minDuration, maxSize) {
        let speed;
        if (!data) {
            data = new Array(128 * 1000).join("abcdefghijklmnop");
        }
        const stClass = this;
        const startTime = performance.now();
        return new Promise(function (resolve, reject) {
            stClass.uploadData(data, url).then(function (endTime) {
                const duration = endTime - startTime;
                const size = data.length;
                if ((duration > minDuration * 1000) || (size >= maxSize * 1000 * 1000 * 0.8)) {
                    speed = (size * 8 / 1000 / 1000) / (duration / 1000);
                    resolve(speed);
                } else {
                    resolve(stClass.measureUploadSpeed(url, data + data.slice(0, data.length / 2), minDuration, maxSize));
                }
            }).catch(function (e) {
                reject(e);
            });
        });
    }

    downloadData(url) {
        return new Promise(function (resolve, reject) {
            fetch(url).then(function (response) {
                if (response.ok && response.status === 200) {
                    response.blob().then(function (blob) {
                        resolve({ time: performance.now(), size: blob.size });
                    });
                } else {
                    reject(new Error(`Download test: network response error. URL: ${url}`));
                }
            }).catch(function (e) {
                reject(e);
            });
        });
    }

    measureDownloadSpeed(round, options) {
        let speed;
        const url = options.files[round];
        const stClass = this;
        const startTime = performance.now();
        return new Promise(function (resolve, reject) {
            stClass.downloadData(url).then(function (result) {
                const duration = result.time - startTime;
                if (duration > options.minDuration * 1000 || round === options.files.length - 1) {
                    speed = (result.size * 8 / 1024 / 1024) / (duration / 1000);
                    resolve(speed);
                } else {
                    resolve(stClass.measureDownloadSpeed(round + 1, options));
                }
            }).catch(function (e) {
                reject(e);
            });
        });
    }
}
