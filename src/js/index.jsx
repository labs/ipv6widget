import "@babel/polyfill";
import { h, render } from "preact";
import settings from "../../settings.json";
import i18n from "../i18n.json";
import Widget from "./components/Widget.jsx";
import Logger from "./utils/logger.jsx";

new Logger(settings.sentryDSN);

class IPv6Widget {
    loadSkin(name, url, callback) {
        const link = document.createElement("link");
        link.type = "text/css";
        link.rel = "stylesheet";
        link.href = `${url}ipv6widget-${name}.css`;
        document.head.appendChild(link);
        link.onload = callback;
        link.onerror = function () {
            throw Error(`Error loading skin ${link.href}`);
        };
    }

    renderWidget(element, options) {
        render(
            <Widget
                options={options}
                testSettings={settings.tests}
                speedSettings={settings.speed}
                i18n={i18n}
            />, element);
    }

    constructor(options) {
        // find root element:
        const root = document.querySelector(options.element);

        // load CSS skin:
        if (options.skin !== false) {
            const name = options.skin || "default";
            // render the widget after skin is loaded:
            this.loadSkin(name, options.url, this.renderWidget(root, options));
        } else {
            // render the widget right away when no skin requested:
            this.renderWidget(root, options);
        }
    }
}

// attach to window object for use in external scripts
window.IPv6Widget = IPv6Widget;
