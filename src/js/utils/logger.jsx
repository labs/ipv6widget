export default class Logger {
    constructor(dsn) {
        this.ravenInstalled = !!window.Raven;
    }

    log(e) {
        if (this.ravenInstalled) {
            window.Raven.captureException(e);
            console.log(e); // eslint-disable-line no-console
        } else {
            throw e;
        }
    }
}
