const webpack = require("webpack");
const path = require("path");

function getPlugins() {
    const plugins = [new webpack.NoEmitOnErrorsPlugin()];

    plugins.push(new webpack.DefinePlugin({
        "process.env": {
            NODE_ENV: process.env.NODE_ENV
        }
    }));

    if (!process.env.NODE_ENV === "production") {
        plugins.push(new webpack.HotModuleReplacementPlugin());
    }

    return plugins;
}

module.exports = {
    mode: process.env.NODE_ENV === "production" ? "production" : "development",
    module: {
        rules: [{
            test: /\.jsx?$/,
            exclude: /node_modules/,
            loader: "babel-loader"
        }]
    },
    devtool: "source-map",
    devServer: {
        contentBase: "build",
        hot: true,
        progress: true
    },
    plugins: getPlugins(),
    stats: {
        colors: true
    },
    entry: {
        ipv6widget: "./src/js/index.jsx",
        cznicwidget: "./src/js/wrapper.js"
    },
    externals: {
        "raven-js": "Raven"
    },
    output: {
        filename: "[name].js",
        path: path.join(__dirname, "build/")
    },
    optimization: {
        minimize: process.env.NODE_ENV === "production"
    }
};
