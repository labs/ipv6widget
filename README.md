# IPv6 Widget

> Modular & extendable JS widget for detecting internet connection capabilities and speed.

<!-- TOC depthFrom:2 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [Features](#features)
- [Technologies & libs](#technologies-libs)
- [License](#license)
- [Bug reporting](#bug-reporting)
- [Using in a webpage](#using-in-a-webpage)
	- [Compatibility with the old widget](#compatibility-with-the-old-widget)
	- [`<iframe>`](#iframe)
	- [`<script>`](#script)
	- [Options](#options)
- [Building](#building)
	- [Dependencies](#dependencies)
	- [Development](#development)
	- [Production](#production)
	- [Distribution archive](#distribution-archive)
	- [Source maps](#source-maps)
- [Deployment](#deployment)
- [Skins](#skins)
	- [Creating your own skin](#creating-your-own-skin)
		- [Using images & custom fonts](#using-images-custom-fonts)
		- [Using external skin](#using-external-skin)
- [Tests](#tests)
	- [Creating a new test](#creating-a-new-test)
		- [Custom states](#custom-states)
		- [Example – useless test with random result after random timeout](#example-useless-test-with-random-result-after-random-timeout)
		- [Retrying](#retrying)

<!-- /TOC -->

## Features

- Included tests:
    - DNSSEC with ECDSA support
    - IPv6
    - [FENIX](http://fe.nix.cz) network detection
- Included speed tests:
    - IPv4 download/upload
    - IPv6 download/upload
- Custom skins
- Multiple language support
- Easily extendable

## Technologies & libs

- ES6
- [Preact](https://preactjs.com/)
- [fetch](https://github.com/github/fetch)
- [SCSS](http://sass-lang.com/)
- Build tools:
    - [Webpack](https://webpack.github.io/)
    - [Babel](https://babeljs.io/)
    - [Gulp](http://gulpjs.com/)

## License

GPL-3.0

## Bug reporting

[GitLab: IPv6Widget issues](https://gitlab.labs.nic.cz/labs/ipv6widget/issues)

Uncaught exceptions are logged into [Sentry project (sentry.labs.nic.cz/CZ-NIC/ipv6widget)](https://sentry.labs.nic.cz/CZ-NIC/ipv6widget/) using [raven-js](https://github.com/getsentry/raven-js). Raven is not included in the widget build itself, it's downloaded during build. It needs to be loaded *before* `ipv6widget.js`:

```html
<script src="raven.js"></script>
```

Sentry DSN (key and URL for remote logger) is set in `settings.json`:

```json
{
    "sentryDSN": "https://xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx@sentry.labs.nic.cz/8"
}
```

It also works with hosted Sentry at [getsentry.com](https://getsentry.com), just change the DSN.

If Raven is not loaded (and/or `sentryDSN` is not set/invalid), exceptions are just logged into browser console.

## Using in a webpage

### Compatibility with the old widget

When including `cznicwidget.js`, the widget is loaded through wrapper which translates old widget init function into a new one, so everything should stay more or less the same for websites which haven't moved to the new version.

### `<iframe>`

Include bundled `index.html` file in your page:

```html
<iframe src="……/ipv6widget/index.html"></iframe>
```

Default options will result in the same functionality as with the old (pre-rewrite) widget:

- IPv6 detection
- DNSSEC detection (but extended with ECDSA support)
- IPv4 and v6 speedtests
- default skin

You can override these options by passing custom values as GET parameters, for example:

```html
<iframe src="……/ipv6widget/index.html?skin=cz.nic&tests=ipv6&tests=dnsec&tests=fenix&speed=ipv6"></iframe>
```

### `<script>`

If you need to add some custom logic for the widget options, it's easier to include the script and call it after the page is loaded:

```html
<script src="ipv6widget.js"></script>
<script>
    var options = {} // see below
    document.addEventListener("DOMContentLoaded", function() {
        new IPv6Widget(options);
    });
</script>
```

### Options

- `element` – *mandatory*
    - selector of widget container
    - eg. `"#ipv6widget"`
- `lang` – *optional*
    - widget translation, 2-letter language code
    - if not set, auto detection (using page and browser language) will be used
    - switches to English if an unsupported language is set (or detected)
    - see `src/i18n.json` for supported languages
    - eg. `"cs"`
- `skin` – *optional*
    - name of desired skin, or `false` for no skin
    - if not set, `default` skin will be used
    - see `src/style/skins/` for a list of included skins
    - eg. `"cz.nic"` or `false`
    - see [Skins](#skins) section of this file for more details and info about creating a custom skin
- `header` - *optional*
    - don't render widget header if set to `false`
- `tests` – *optional*
    - list of tests to include (ordered)
    - see `src/js/tests` for a list of included tests
    - eg. `["ipv6", "dnssec", "fenix"]`
    - if not defined or empty, Test component won't be rendered at all
    - see [Tests](#tests) section of this file for more details and info about creating a new test
- `speed` – *optional*
    - list of speed tests to include (ordered)
    - see `src/js/speed` for a list of included speedtests
    - eg. `["ipv4", "ipv6"]`
    - if not defined or empty, Speed component won't be rendered at all


Example:

```javascript
new IPv6Widget({
    element: "#ipv6widget",
    tests: ["ipv6", "dnssec", "fenix"],
    speed: ["ipv4", "ipv6"],
    lang: "es",
    skin: "cz.nic"
});
```

## Building

Builds are created automatically by GitLab CI, and are available at [gitlab.labs.nic.cz/labs/ipv6widget/builds](https://gitlab.labs.nic.cz/labs/ipv6widget/builds?scope=finished).

Everything works with [Yarn package manager](https://yarnpkg.com/), too (and it's a little bit faster). Feel free to use `yarn` instead of `npm` if you prefer…

### Dependencies

This project uses NodeJS tools ([gulp](http://gulpjs.com/), [webpack](https://webpack.github.io/), [Babel](http://babeljs.io/)) for building. Running `npm install` will fetch everything that's needed.

Depending on your shell configuration, you might need to add `node_modules/.bin` directory to `$PATH` if you're getting errors like "gulp not found":

```bash
export PATH="${PATH}:`pwd`/node_modules/.bin"
```

### Development

Run `npm start` to run a development server on port 3000. Features:

- watches for file changes in `src/`, automatically rebuild what's needed and reloads page in browser(s) (except when only CSS changes – then it's injected without reloading)
- user actions are synchronized over all browsers (even on multiple devices)
- [Browsersync](https://www.browsersync.io/) web UI on port 3001 with some neat options for debugging etc.

#### Note on IPv6-only networks

Webpack's dev server crashes sometimes when there is no ipv4 address on the network interface. The widget itself works fine, but it's more convenient to try it with some other HTTP server, eg. the one from Python:

```
$ cd build
$ python -m http.server 3000   # or -m SimpleHTTPServer for Python 2.x
```

### Production

Running `npm run build` will create a minimized production build ready for deployment. This is done by GitLab CI after each push, builds are available here: https://gitlab.labs.nic.cz/labs/ipv6widget/builds?scope=finished.

### Distribution archive

Running `npm run build:tbz2` will clean the build directory, make a new minimized build and pack it into ipv6widget.tbz2.

### Source maps

Source map files (`.js.map`, `.css.map`) are produced by both development and production build. It's useful to put them on production server, too, especially if Sentry logging is enabled (Sentry fetches the map to show exception origin).

Source maps are quite large (about 4-5× the JS file size), but browsers generally don't download them until dev tools are opened:

```
Page with widget loaded:
[11:41:03] "GET / HTTP/1.1" 200 -
[11:41:03] "GET /ipv6widget.js HTTP/1.1" 200 -
[11:41:03] "GET /ipv6widget-cz.nic.css HTTP/1.1" 200 -
Chrome dev tools opened:
[11:41:09] "GET /ipv6widget.js.map HTTP/1.1" 200
```

## Deployment

Currently there is sadly no automated prodecure to get the build to the production server. If you need to deploy a new version, create a ticket in [CZ.NIC Trac](admin.nic.cz) with the owner set to *admins_nic*.

The procedure itself is pretty straightforward – the content of `build` directory (inside the archive downloaded [from GitLab](https://gitlab.labs.nic.cz/labs/ipv6widget/builds?scope=finished)) needs to be unpacked to `test-ipv6.cz/ipv6widget/`.

The master branch builds are published to [GitLab Pages](https://labs.pages.labs.nic.cz/ipv6widget/), so there's a fully functional and always up-to-date version there. It won't work in `iframe`s however – Gitlab sets `X-frame-options` to `DENY` (and `SAMEORIGIN`, the header is duplicated for some reason)…

## Skins

Widget can be skinned with SCSS (and optionally images). To use a skin, pass it's name to `new IPv6Widget` like this:

```javascript
new IPv6Widget({
    element: "#ipv6widget",
    skin: "cz.nic",
    tests: ["ipv6", "dnssec", "fenix"]
});
```

### Creating your own skin

Create a new directory under `src/skins/`. Entry point is `src/skins/[name]/skin-[name].scss`.

So if you want your skin to be named `dotted-unicorn`:

```bash
$ mkdir src/skins/dotted-unicorn
$ touch src/skins/dotted-unicorn/skin-dotted-unicorn.scss
```

Add your skin to `gulpfile.babel.js` to build it:

```javascript
const skins = ["default", "dotted-unicorn"];
```

Create any file structure you want in `src/skins/dotted-unicorn/` – eg. it's quite handy to define color variables in a separate imported file for easier changes.

You can even import some other skin and extend/change it:

```css
@import "../skin-default/widget";

.ipv6widget {
    background: url("https://unicorn.me/dotted.png");
}
```

#### Using images & custom fonts

There are Gulp tasks ready for bundling custom images and fonts:

- images from `src/img/` are optimized and copied to `build/img/`
- fonts from `src/fonts` are copied to `src/fonts/`

… so it's easy to use them with CSS `url(…)` notation.

However, please remember that every file creates an additional HTTP request. For small images, it's possible to overcome this using data URIs (base64-encoded files in `url()`) or CSS sprites (multiple small images merged into a big one and displayed with `background-position`).

#### Using external skin

Skins are just CSS styles, so it's possible to supply your own style without creating a custom build of this widget.

Setting `skin` option to `false` disables loading of the default skin:

```javascript
new IPv6Widget({
    element: "#ipv6widget",
    skin: false,
    tests: ["ipv6", "dnssec", "fenix"]
});
```

Then just load your own style (`<link rel="stylesheet" href="…" />`).

## Tests

### Creating a new test

Test classes are dynamically loaded from `src/js/tests/`. So, to create a new test, create file in this directory and pass it's name to `new IPv6Widget` call like so:

```javascript
new IPv6Widget({
    element: "#ipv6widget",
    tests: ["my_test"] // will use class from src/js/tests/my_test.jsx
});
```

Only one function is mandatory in the test class – `getState()`, which should return a `Promise` resolving to a string containing the desired state of test block:

```javascript
getState(options) {
    return new Promise(function(resolve, reject) {
        let result = doSomething(options.url);
        if result === True {
            resolve("ok");
        } else {
            resolve("fail");
        }
    });
}
```

The `options` parameter is a section for given test read from `settings.json`. It's useful for storing backend URLs and similar settings.

If not set otherwise, initial state is `loading`, returned by `getInitialState()` function in `Test` class (`src/js/tests/Test.jsx`).

You can set another `initialState` in `settings.json`:

```json
{
    "tests": {
        "my_test": {
            "url": "…",
            "initialState": "off"
        }
    }
}
```

If you need some logic behind the initial state, free to override the inherited function with your own:

```javascript
export default class MyTest extends Test {
    getInitialState(options) {
        let result = doSomething();
        if result === True {
            return "loading";
        } else {
            return "off";
        }
    }
}
```

Indicator will use the returned state from the beginning, and change it as soon as Promise returned from `getState()` is resolved.

Returning Promises from `getInitialState()` is currently not supported, as it should return the state (near) instantly.

Again, `options` parameter is taken from `settings.json`.

#### Custom states

It's possible to return a custom state if needed – just be sure to create it's class in a skin file. See `src/tests/dnssec.jsx` for example – it results in *partial* state if one of RSA/ECDSA test fails.

#### Example – useless test with random result after random timeout

Create a new file `random.jsx` in `src/js/tests/`:

```javascript
import Test from "./Test.jsx";

export default class Random extends Test {
    getInitialState(options) {   // getInitialState() is not needed, as it's inherited from the Test class…
        return "loading"; // …but feel free to override it and set some other initial state if you want.
    }

    getState(options) {
        return new Promise(function(resolve, reject) { // getState() must return a Promise
            let r = Math.random();
            setTimeout(function() {
                if (r > options.threshold) {
                    resolve("ok");  // resolve(…) passes the state to TestBlock component
                } else {
                    resolve("fail");
                }
            }, r * 3000);
        });
    }
}
```

Add translations to `src/i18n.json`:

```json
"random": {
    "label": {
        "en": "Random",
        "cs": "…",
        "…": "…"
    },
    "ok": {
        "…": "…"
    },
    "…": {
        "…": "…"    
    }
},
```

Add a section to `settings.json` if needed:

```json
{
    "tests": {
        "random": {
            "threshold": 0.5
        }
    }
}
```

Then just pass the test name in options:

```javascript
new IPv6Widget({
    element: "#ipv6widget",
    tests: ["ipv6", "dnssec", "random"]
});
```

#### Retrying

There is no retry mechanism built-in, as it's not very useful for included tests (eg. ipv6 won't magically start working if we try it again in five seconds…).

However, it's easy to integrate it in your own test class – just call `resolve` with `getState()` as a value:

```javascript
export default class my_test extends Test {
    getState(options, retries) {
        var my_test = this;
        if (!retries) var retries = 0;
        return new Promise(function(resolve, reject) {
            if (doSomething()) {
                resolve("ok");
            } else {
                resolve("fail");
            }
        }).catch(function(e) {
            retries++;
            if (retries <= options.retries) {
                resolve(my_test.getState(options, retries));
            } else {
                resolve("off");
            }
        })
    }
}
```
