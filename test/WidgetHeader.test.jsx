const render = require("preact-render-to-string");
import i18n from "../src/i18n.json";
import WidgetHeader from "../src/js/components/WidgetHeader.jsx";

describe("Widget header", () => {
    Object.keys(i18n.header).map(lang => {
        it(`matches snapshot with lang: ${lang}`, () => {
            const tree = render(
                <WidgetHeader label={i18n.header} lang={lang} />
            );
            expect(tree).toMatchSnapshot();
        });
    });
});
