const render = require("preact-render-to-string");
import settings from "../settings.json";
import i18n from "../src/i18n.json";
import Widget from "../src/js/components/Widget.jsx";
import "isomorphic-fetch";

const langs = Object.keys(i18n.header);

const testCombinations = [
    [],
    ["ipv6"],
    ["fenix"],
    ["dnssec"],
    ["ipv6", "fenix"],
    ["ipv6", "dnssec"],
    ["ipv6", "fenix", "dnssec"],
    ["fenix", "dnssec"]
];

const speedCombinations = [
    [],
    ["ipv4"],
    ["ipv6"],
    ["ipv4", "ipv6"]
];

describe("Widget renders with", () => {
    langs.map(lang => {
        describe(`lang: ${lang}`, () => {
            testCombinations.map(tests => {
                describe(`tests: ${tests}`, () => {
                    speedCombinations.map(speed => {
                        it(`speed: ${speed}`, () => {
                            const tree = render(
                                <Widget
                                    options={{
                                        lang,
                                        tests,
                                        speed
                                    }}
                                    testSettings={settings.tests}
                                    speedSettings={settings.speed}
                                    i18n={i18n}
                                />
                            );
                            expect(tree).toMatchSnapshot();
                        });
                    });
                });
            });
        });
    });
});
