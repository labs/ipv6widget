import "@babel/polyfill";
import puppeteer from "puppeteer";
import { toMatchImageSnapshot } from "jest-image-snapshot";
const { teardown: teardownDevServer } = require("jest-dev-server");
const { setup: setupDevServer } = require("jest-dev-server");

const urls = {
    default:
        "http://localhost:3000/?tests=ipv6&tests=dnssec&tests=fenix&skin=default",
    cznic:
        "http://localhost:3000/?tests=ipv6&tests=dnssec&tests=fenix&skin=cz.nic",
    speed_default:
        "http://localhost:3000/?speed=ipv4&speed=ipv6&skin=default&jest",
    speed_cznic: "http://localhost:3000/?speed=ipv4&speed=ipv6&skin=cz.nic&jest"
};

const matchOptions = {
    failureThreshold: 1.5, // tolerance for different font rendering in CI, loader animations etc.
    failureThresholdType: "percent"
};

const timeout = 10 * 1000;

const browserOptions = {
    args: ["--no-sandbox", "--disable-setuid-sandbox"]
};

const viewport = {
    width: 640,
    height: 640
};

describe("Browser renders", () => {
    expect.extend({ toMatchImageSnapshot }, timeout);

    beforeAll(async done => {
        await setupDevServer({
            command: "http-server build -p 3000",
            protocol: "http",
            launchTimeout: 3000,
            usedPortAction: "kill",
            port: 3000
        });
        done();
    });

    afterAll(async done => {
        await teardownDevServer();
        done();
    });

    it(
        "(default skin) the initial state correctly",
        async () => {
            const browser = await puppeteer.launch(browserOptions);
            const tab = await browser.newPage();
            await tab.setViewport(viewport);
            await tab.goto(urls.default);
            const screenshot = await tab.screenshot();

            expect(screenshot).toMatchImageSnapshot(matchOptions);

            await browser.close();
        },
        timeout
    );

    it(
        "(default skin) IPv6 test info screen correctly",
        async () => {
            const browser = await puppeteer.launch(browserOptions);
            const tab = await browser.newPage();
            await tab.setViewport(viewport);
            await tab.goto(urls.default);
            await tab.waitFor(1000);
            await tab.click(".test:nth-child(1) .info-button");
            const screenshot = await tab.screenshot();

            expect(screenshot).toMatchImageSnapshot(matchOptions);

            await browser.close();
        },
        timeout
    );

    it(
        "(default skin) Fenix test info screen correctly",
        async () => {
            const browser = await puppeteer.launch(browserOptions);
            const tab = await browser.newPage();
            await tab.setViewport(viewport);
            await tab.goto(urls.default);
            await tab.waitFor(1000);
            await tab.click(".test:nth-child(3) .info-button");
            const screenshot = await tab.screenshot();

            expect(screenshot).toMatchImageSnapshot(matchOptions);

            await browser.close();
        },
        timeout
    );

    it(
        "(default skin) speed test correctly",
        async () => {
            const browser = await puppeteer.launch(browserOptions);
            const tab = await browser.newPage();
            await tab.setViewport(viewport);
            await tab.goto(urls.speed_default);
            await tab.waitFor(1000);
            await tab.click(".speed-button");
            const screenshot = await tab.screenshot();

            expect(screenshot).toMatchImageSnapshot(matchOptions);

            await browser.close();
        },
        timeout
    );

    it(
        "(cz.nic skin) the initial state correctly",
        async () => {
            const browser = await puppeteer.launch(browserOptions);
            const tab = await browser.newPage();
            await tab.setViewport(viewport);
            await tab.goto(urls.cznic);
            const screenshot = await tab.screenshot();

            expect(screenshot).toMatchImageSnapshot(matchOptions);

            await browser.close();
        },
        timeout
    );

    it(
        "(cz.nic skin) IPv6 test info screen correctly",
        async () => {
            const browser = await puppeteer.launch(browserOptions);
            const tab = await browser.newPage();
            await tab.setViewport(viewport);
            await tab.goto(urls.cznic);
            await tab.waitFor(1000);
            await tab.click(".test:nth-child(1) .info-button");
            const screenshot = await tab.screenshot();

            expect(screenshot).toMatchImageSnapshot(matchOptions);

            await browser.close();
        },
        timeout
    );

    it(
        "(cz.nic skin) Fenix test info screen correctly",
        async () => {
            const browser = await puppeteer.launch(browserOptions);
            const tab = await browser.newPage();
            await tab.setViewport(viewport);
            await tab.goto(urls.cznic);
            await tab.waitFor(1000);
            await tab.click(".test:nth-child(3) .info-button");
            const screenshot = await tab.screenshot();

            expect(screenshot).toMatchImageSnapshot(matchOptions);

            await browser.close();
        },
        timeout
    );

    it(
        "(cz.nic skin) speed test correctly",
        async () => {
            const browser = await puppeteer.launch(browserOptions);
            const tab = await browser.newPage();
            await tab.setViewport(viewport);
            await tab.goto(urls.speed_cznic);
            await tab.waitFor(1000);
            await tab.click(".speed-button");
            const screenshot = await tab.screenshot();

            expect(screenshot).toMatchImageSnapshot(matchOptions);

            await browser.close();
        },
        timeout
    );
});
