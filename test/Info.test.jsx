const render = require("preact-render-to-string");
import settings from "../settings.json";
import i18n from "../src/i18n.json";
import Info from "../src/js/components/Info.jsx";

let testsWithInfo = [];

Object.keys(settings.tests).map(testName => {
    if (Object.keys(settings.tests[testName]).includes("info")) {
        testsWithInfo.push(testName);
    }
});

describe("Info screen renders with", () => {
    testsWithInfo.map(testName => {
        describe(`test: ${testName}`, () => {
            Object.keys(i18n.tests[testName].info).map(testState => {
                describe(`state: ${testState}`, () => {
                    Object.keys(i18n.tests[testName].info[testState]).map(lang => {
                        it(`lang: ${lang}`, () => {
                            const tree = render(
                                <Info
                                    i18n={i18n.tests[testName]}
                                    testState={testState}
                                    lang={lang}
                                />
                            );
                            expect(tree).toMatchSnapshot();
                        });
                    });
                });
            });
        });
    });
});
